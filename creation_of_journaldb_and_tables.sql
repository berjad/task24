create database journaldb;

use journaldb;

create table author (
	id INT PRIMARY KEY AUTO_INCREMENT NOT NULL,
    first_name VARCHAR(80) NOT NULL,
    last_name VARCHAR(128) NOT NULL,
    email VARCHAR(128),
    created_at DATETIME NOT NULL DEFAULT NOW(),
    updated_at DATETIME DEFAULT NULL ON UPDATE NOW()
);

create table journal (
	id INT PRIMARY KEY AUTO_INCREMENT NOT NULL,
    title VARCHAR(128) NOT NULL,
	created_at DATETIME NOT NULL DEFAULT NOW(),
    updated_at DATETIME DEFAULT NULL ON UPDATE NOW()
);

create table journal_entry (
	id INT PRIMARY KEY AUTO_INCREMENT NOT NULL,
    title VARCHAR(128) NOT NULL,
    content TEXT,
    created_at DATETIME NOT NULL DEFAULT NOW(),
    updated_at DATETIME DEFAULT NULL ON UPDATE NOW(),
    author_id INT NOT NULL,
    journal_id INT NOT NULL,
    FOREIGN KEY (author_id) REFERENCES author(id),
    FOREIGN KEY (journal_id) REFERENCES journal(id)
);

create table tag (
	id INT PRIMARY KEY AUTO_INCREMENT NOT NULL,
    title VARCHAR(128) NOT NULL, 
	created_at DATETIME NOT NULL DEFAULT NOW(),
    updated_at DATETIME DEFAULT NULL ON UPDATE NOW()
);

create table journal_entry_tag (
	id INT PRIMARY KEY AUTO_INCREMENT NOT NULL,
    tag_id INT,
    journal_entry_id INT,
    FOREIGN KEY (tag_id) REFERENCES tag(id),
    FOREIGN KEY (journal_entry_id) REFERENCES journal_entry(id),
    created_at DATETIME NOT NULL DEFAULT NOW(),
    updated_at DATETIME DEFAULT NULL ON UPDATE NOW()
);

alter table tag
change update_at update_at DATETIME DEFAULT NULL ON UPDATE NOW();
