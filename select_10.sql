SELECT author.first_name, author.last_name, author.email, author.created_at, journal_entry.title, journal_entry.text, journal_entry.author_id, journal_entry.journal_id
FROM journal_entry
LEFT JOIN author ON journal_entry.id = author.id