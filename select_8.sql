SELECT journal.name, journal.created_at, journal_entry.title, journal_entry.text
FROM journal_entry
LEFT JOIN journal ON journal_entry.id = journal.id