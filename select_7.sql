SELECT author.first_name, author.last_name, journal_entry.title, journal_entry.text
FROM journal_entry
LEFT JOIN author ON journal_entry.id = author.id