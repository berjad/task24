SELECT author.first_name, author.last_name, author.email, tag.title, tag.created_at
FROM tag
LEFT JOIN author ON tag.id = author.id